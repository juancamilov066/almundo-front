import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Urls } from '../constants/Urls';
import { RemoteDataService } from '../interfaces/RemoteDataService';
import { DataResponse } from '../responses/Responses';
import { Hotel } from '../models/Hotel.model';
import { JsonConvert } from 'json2typescript';
import { environment } from '../../../environments/environment';

@Injectable()
export class HotelService implements RemoteDataService {

    private jsonConvert: JsonConvert;

    constructor(private http: HttpClient) {
        this.jsonConvert = new JsonConvert();
    }

    findAll(page: number, name: string, stars?: Array<number>): Observable<DataResponse<Array<Hotel>>> {

        let params = new HttpParams()
            .append('page', String(page))
            .append('name', name)
            .append('sendJson', environment.production ? '0' : '1');

        if (stars !== undefined) {
            stars.forEach(star => {
                params = params.append('stars[]', String(star));
            });
        }

        return this.http.get(Urls.FIND_ALL_HOTELS, {
            params: params
        }).pipe(
            map((data: any) => {
                return new DataResponse(data.code, this.jsonConvert.deserialize(data.data, Hotel)
                    , data.success, data.message);
            }),
            catchError(this.handleError)
        );
    }

    findById(id: number): Observable<DataResponse<Hotel>> {
        return this.http.get(Urls.FIND_HOTEL_BY_ID(id)).pipe(
            map((data: any) => {
                return new DataResponse(data.code, this.jsonConvert.deserialize(data.data, Hotel)
                    , data.success, data.message);
            }),
            catchError(this.handleError)
        );
    }

    handleError(error: any) {
        return throwError(error);
    }
}
