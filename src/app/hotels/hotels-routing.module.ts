import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HotelSearchComponent } from './hotel-search/hotel-search.component';
import { HotelDetailComponent } from './hotel-detail/hotel-detail.component';

const routes: Routes = [{
    path: '', component: HotelSearchComponent
}, {
    path: ':id', component: HotelDetailComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HotelsRoutingModule { }
