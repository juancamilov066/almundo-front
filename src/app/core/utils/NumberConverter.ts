import { JsonConverter, JsonCustomConvert } from 'json2typescript';

@JsonConverter
export default class NumberConverter implements JsonCustomConvert<Number> {

    serialize(number: Number): any {
        return number;
    }
    deserialize(number: any): Number {
        return number === null || number === undefined ? null : Number(number);
    }

}
