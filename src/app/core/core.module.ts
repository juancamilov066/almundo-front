import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HotelService } from './services/Hotel.service';

@NgModule({
    imports: [
      CommonModule
    ],
    declarations: [
    ],
    providers: [
        HotelService
    ]
})
export class CoreModule { }
