import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Hotel } from '../../core/models/Hotel.model';

@Component({
  selector: 'app-hotel-list-item',
  templateUrl: './hotel-list-item.component.html',
  styleUrls: ['./hotel-list-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HotelListItemComponent implements OnInit {

    @Input()
    hotel: Hotel;
    picPath: string;
    starsArray: Array<any>;

    constructor() { }

    ngOnInit() {
        this.picPath = 'http://localhost:3000/assets/hotel/' + this.hotel.$image;
        this.starsArray = new Array(this.hotel.$stars);
    }

}
