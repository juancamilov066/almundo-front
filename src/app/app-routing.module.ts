import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [{
    path: '', redirectTo: 'hotels', pathMatch: 'full'
}, {
    path: 'hotels', loadChildren: './hotels/hotels.module#HotelsModule'
}];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
