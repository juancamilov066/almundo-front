export class DataResponse<T> {

    private code: number;
    private data: T;
    private success: boolean;
    private message: string;

    constructor(code: number, data: T, success: boolean, message: string) {
        this.code = code;
        this.data = data;
        this.success = success;
        this.message = message;
    }


    /**
     * Getter $code
     * @return {number}
     */
    public get $code(): number {
        return this.code;
    }

    /**
     * Setter $code
     * @param {number} value
     */
    public set $code(value: number) {
        this.code = value;
    }

    /**
     * Getter $data
     * @return {T}
     */
    public get $data(): T {
        return this.data;
    }

    /**
     * Setter $data
     * @param {T} value
     */
    public set $data(value: T) {
        this.data = value;
    }

    /**
     * Getter $success
     * @return {boolean}
     */
    public get $success(): boolean {
        return this.success;
    }

    /**
     * Setter $success
     * @param {boolean} value
     */
    public set $success(value: boolean) {
        this.success = value;
    }

    /**
     * Getter $message
     * @return {string}
     */
    public get $message(): string {
        return this.message;
    }

    /**
     * Setter $message
     * @param {string} value
     */
    public set $message(value: string) {
        this.message = value;
    }


}
