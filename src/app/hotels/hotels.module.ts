import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HotelsRoutingModule } from './hotels-routing.module';
import { HotelSearchComponent } from './hotel-search/hotel-search.component';
import { HotelListItemComponent } from './hotel-list-item/hotel-list-item.component';
import { HotelDetailComponent } from './hotel-detail/hotel-detail.component';
import { HotelsComponent } from './hotels.component';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        ReactiveFormsModule,
        HotelsRoutingModule,
        MatCheckboxModule,
        LazyLoadImageModule,
        InfiniteScrollModule,
        MatProgressSpinnerModule
    ],
    declarations: [
        HotelSearchComponent,
        HotelListItemComponent,
        HotelDetailComponent,
        HotelsComponent
    ]
})
export class HotelsModule { }
