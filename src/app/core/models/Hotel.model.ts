import { JsonObject, JsonProperty } from 'json2typescript';
import { Amenity } from './Amenity.model';
import NumberConverter from '../utils/NumberConverter';
@JsonObject
export class Hotel {

    @JsonProperty('id', NumberConverter)
    private id: number = undefined;

    @JsonProperty('name', String)
    private name: string = undefined;

    @JsonProperty('stars', Number)
    private stars: number = undefined;

    @JsonProperty('price', Number)
    private price: number = undefined;

    @JsonProperty('image', String)
    private image: string = undefined;

    @JsonProperty('amenities', [])
    private amenities: Array<any> = undefined;

    constructor() {}

    /**
     * Getter $id
     * @return {number }
     */
    public get $id(): number  {
        return this.id;
    }

    /**
     * Setter $id
     * @param {number } value
     */
    public set $id(value: number ) {
        this.id = value;
    }

    /**
     * Getter $name
     * @return {string }
     */
    public get $name(): string  {
        return this.name;
    }

    /**
     * Setter $name
     * @param {string } value
     */
    public set $name(value: string ) {
        this.name = value;
    }


    /**
     * Getter $stars
     * @return {number }
     */
    public get $stars(): number  {
        return this.stars;
    }

    /**
     * Setter $stars
     * @param {number } value
     */
    public set $stars(value: number ) {
        this.stars = value;
    }

    /**
     * Getter $price
     * @return {number }
     */
    public get $price(): number  {
        return this.price;
    }

    /**
     * Setter $price
     * @param {number } value
     */
    public set $price(value: number ) {
        this.price = value;
    }

    /**
     * Getter $amenities
     * @return {Array<Amenity>}
     */
    public get $amenities(): Array<any> {
        return this.amenities;
    }

    /**
     * Setter $amenities
     * @param {Array<Amenity>} value
     */
    public set $amenities(value: Array<any>) {
        this.amenities = value;
    }

    /**
     * Getter $image
     * @return {string }
     */
    public get $image(): string  {
        return this.image;
    }

    /**
     * Setter $image
     * @param {string } value
     */
    public set $image(value: string ) {
        this.image = value;
    }


}
