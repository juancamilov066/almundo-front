import { JsonProperty, JsonObject } from 'json2typescript';
import NumberConverter from '../utils/NumberConverter';
@JsonObject
export class Amenity {

    @JsonProperty('id', NumberConverter)
    private id: number = undefined;

    @JsonProperty('name', String)
    private name: string = undefined;

    constructor() {}

    /**
     * Getter $id
     * @return {number }
     */
    public get $id(): number  {
        return this.id;
    }

    /**
     * Setter $id
     * @param {number } value
     */
    public set $id(value: number ) {
        this.id = value;
    }

    /**
     * Getter $name
     * @return {string }
     */
    public get $name(): string  {
        return this.name;
    }

    /**
     * Setter $name
     * @param {string } value
     */
    public set $name(value: string ) {
        this.name = value;
    }

}
