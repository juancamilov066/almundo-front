# Almundo Front End - Prueba

Hola, bienvenido a la prueba de AlMundo,
Este repositorio contiene el proyecto front end de la prueba

## Version de Angular usada
6.0.5

## Instrucciones para ejecutar

Antes de empezar, desde la carpeta del proyecto, usa el comando `npm install` para instalar todas la dependencias del proyecto, luego corre `ng serve`, asegurate de tener el Angular CLI instalado y el package manager de NPM.
Al ejecutarlo abre tu navegador en la direccion `http://localhost:4200/`
Y eso es todo :)

## Informacion extra

El proyecto esta configurado como PWA, por lo tanto, si se subiese a un servidor y se accediera por medio de un dominio con HTTPS se podria instalar en un smartphone

