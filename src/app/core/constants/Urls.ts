import { environment } from '../../../environments/environment';

export class Urls {

    private static readonly BASE_URL: string = environment.production ? 'http://localhost:3000' : 'http://localhost:3000';
    public static readonly FIND_ALL_HOTELS: string = `${Urls.BASE_URL}/hotel`;

    public static FIND_HOTEL_BY_ID(id: number) {
        return `${this.BASE_URL}/hotel/${id}`;
    }

}
