import { Component, OnInit, HostListener, ChangeDetectionStrategy } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { FormControl, FormArray } from '@angular/forms';
import { HotelService } from '../../core/services/Hotel.service';
import { Subscription } from 'rxjs';
import { DataResponse } from '../../core/responses/Responses';
import { Hotel } from '../../core/models/Hotel.model';

@Component({
    selector: 'app-hotel-search',
    templateUrl: './hotel-search.component.html',
    styleUrls: ['./hotel-search.component.scss'],
    animations: [
        trigger('isFilterOpen', [
            state('show', style({
                height: '300px',
                opacity: 1
            })),
            state('hide', style({
                height: '0px',
                opacity: 0,
                display: 'none'
            })),
            transition('show => hide', animate('300ms ease-out')),
            transition('hide => show', animate('300ms ease-in'))
        ]),
        trigger('isHotelOpen', [
            state('show', style({
                height: '50px',
                opacity: 1
            })),
            state('hide', style({
                height: '0px',
                opacity: 0,
                display: 'none'
            })),
            transition('show => hide', animate('300ms ease-out')),
            transition('hide => show', animate('300ms ease-in'))
        ]),
        trigger('isStarsOpen', [
            state('show', style({
                height: '180px',
                opacity: 1
            })),
            state('hide', style({
                height: '0px',
                opacity: 0,
                display: 'none'
            })),
            transition('show => hide', animate('300ms ease-out')),
            transition('hide => show', animate('300ms ease-in'))
        ])
    ]
})
export class HotelSearchComponent implements OnInit {

    searchForm = {
        allControl: false,
        fiveStarsControl: false,
        fourStarsControl: false,
        threeStarsControl: false,
        twoStarsControl: false,
        oneStarsControl: false,
        hotelName: ''
    };

    isFilterOpen = false;
    isHotelOpen = true;
    isStarsOpen = true;

    error = undefined;
    notFound = undefined;
    isFilterSearchTriggered = false;
    loading = false;
    page = 0;

    starsFilterArray: Array<number> = new Array();
    subscription: Subscription = new Subscription();
    hotels: Array<Hotel> = new Array<Hotel>();

    constructor(private hotelService: HotelService) {}

    ngOnInit() {
        this.fetchHotels();
    }

    get currentFilterState() {
        return this.isFilterOpen ? 'show' : 'hide';
    }

    get currentStarsState() {
        return this.isStarsOpen ? 'show' : 'hide';
    }

    get currentNameState() {
        return this.isHotelOpen ? 'show' : 'hide';
    }

    toggle(type): void {
        if (type === 'NAME') {
            this.isHotelOpen = !this.isHotelOpen;
        }
        if (type === 'STARS') {
            this.isStarsOpen = !this.isStarsOpen;
        }
        if (type === 'FILTER') {
            this.isFilterOpen = !this.isFilterOpen;
            return;
        }
    }

    fetchHotels(reset?: boolean): void {
        this.error = undefined;
        this.notFound = undefined;
        if (reset === true) {
            this.isFilterSearchTriggered = true;
            this.page = 0;
            this.hotels = new Array<Hotel>();
        }

        this.loading = true;
        this.subscription.add(this.hotelService.findAll(
                this.page,
                this.searchForm.hotelName,
                this.starsFilterArray.length === 0 ? undefined : this.starsFilterArray
            )
            .subscribe((response: DataResponse<Array<Hotel>>) => {
                this.loading = false;
                if (response.$code === 200) {
                    if (response.$data.length === 0 && this.page === 0) {
                        this.notFound = true;
                    }
                    response.$data.map(hotel => this.hotels.push(hotel));
                }
            }, error => {
                this.loading = false;
                this.error = error;
            }));
    }

    onNameChange(event): void {
        this.searchForm.hotelName = event;
        if (event === '' && this.isFilterSearchTriggered) {
            this.fetchHotels(true);
        }
    }

    onStarChanged(isAll: boolean): void {
        this.starsFilterArray = new Array();
        if (isAll === true) {
            this.searchForm.fiveStarsControl = false;
            this.searchForm.fourStarsControl = false;
            this.searchForm.threeStarsControl = false;
            this.searchForm.twoStarsControl = false;
            this.searchForm.oneStarsControl = false;
        }
        if (this.searchForm.fiveStarsControl) { this.starsFilterArray.push(5); }
        if (this.searchForm.fourStarsControl) { this.starsFilterArray.push(4); }
        if (this.searchForm.threeStarsControl) { this.starsFilterArray.push(3); }
        if (this.searchForm.twoStarsControl) { this.starsFilterArray.push(2); }
        if (this.searchForm.oneStarsControl) { this.starsFilterArray.push(1); }
        this.fetchHotels(true);
    }

    onListScroll() {
        this.page++;
        this.fetchHotels();
    }

}
